const { Client } = require('pg');
// eslint-disable-next-line no-unused-vars
const dotenv = require('dotenv').config();

const user = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const url = process.env.DB_URL;

const connectionString: string = `postgres://${user}:${password}@${url}/Catlendar`;

const client = new Client(connectionString);
client.connect();

export default client;
