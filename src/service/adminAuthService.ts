import db from '../database/index';

const adminAuth = async (user: number) => {
  const text = 'SELECT admin FROM users WHERE id=$1';
  const values = [user];
  const admin = await db.query(text, values);
  return admin.rows[0].admin;
};

export default adminAuth;
