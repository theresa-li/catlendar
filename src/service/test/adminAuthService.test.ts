import supertest from "supertest";
import app from '../../index';
import db from '../../database/index';
import adminAuth from "../adminAuthService";

jest.mock('../../database/index');

describe('Admin Auth Service', () => {
  it('authorizes admin', async () => {
    db.query.mockResolvedValue({ rows: [{ admin: true }] });
    const res: any = await adminAuth(1);
    expect(res).toEqual(true);
  });
});
