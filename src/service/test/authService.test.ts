import supertest from "supertest";
import app from '../../index';
import db from '../../database/index';
import authenticate from "../authService";

jest.mock('../../database/index');

describe('Auth Service', () => {
  it('authorizes user', async () => {
    db.query.mockResolvedValue({ rows: [{ id: 1 }] });
    const res: any = await authenticate({ username: 'username', password: 'password' });
    expect(res).toEqual(1);
  });
});
