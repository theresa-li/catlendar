import db from '../database/index';

// Confirms existing username and password combination
const confirmUser = async (username: string, password: string) => {
  const text: string = 'SELECT id FROM users WHERE username=($1) AND password=($2)';
  const values: Array<string> = [username, password];
  const user = await db.query(text, values);
  return user.rows[0].id;
};

const authenticate = async (credentials: any) => {
  if (credentials) {
    if (credentials.username && credentials.password) {
      const username = Buffer.from(credentials.username, 'base64').toString('ascii');
      const password = Buffer.from(credentials.password, 'base64').toString('ascii');
      const user = await confirmUser(username, password);
      if (user) {
        return user;
      }
    }
  }
  return undefined;
};

export default authenticate;
