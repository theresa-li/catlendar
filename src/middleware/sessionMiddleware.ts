import session from 'express-session';

const sessionConfig = {
  secret: 'mySecret',
  resave: true,
  saveUninitialized: true,
};
const sessionMiddleware = session(sessionConfig);

export default sessionMiddleware;
