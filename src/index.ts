/* eslint-disable import/no-unresolved */
/* eslint-disable import/extensions */
// eslint-disable-next-line no-unused-vars
import express, { Application, Request, Response } from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import loginRouter from './router/logInRouter';
import authRouter from './router/authRouter';
import sessionMiddleware from './middleware/sessionMiddleware';
import taskRouter from './router/taskRouter';
import quoteRouter from './router/quoteRouter';

const app: Application = express();

const port: number = 3000;

app.use(bodyParser.json());
app.use(sessionMiddleware);
app.use(cors({credentials: true}));

app.get('/', (req: Request, res: Response) => {
  res.send('Hello world!');
});

app.use('/user', loginRouter);
app.use('/user', authRouter);
app.use('/api/tasks', taskRouter);
app.use('/api/quotes', quoteRouter);


if (process.env.NODE_ENV !== 'test') {
  // eslint-disable-next-line no-console
  app.listen(port, () => console.log(`Server is running on port ${port}`));
}

export default app;
