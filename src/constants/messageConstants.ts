const message = {
  loggedOut: 'You are not logged in.',
  noSession: 'No session in place.',
  noTask: 'The task you are trying to access does not exist.',
  noQuote: 'The quote you are trying to access does not exist.',
  notAdmin: 'You are not authorized.',
};

export default message;
