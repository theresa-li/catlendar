import { Router, Request, Response } from 'express';
import authenticate from '../service/authService';
import adminAuth from '../service/adminAuthService';
import message from '../constants/messageConstants';
import db from '../database/index';

const quoteRouter = Router();

export default quoteRouter;

// [Admin only] Creates a quote
const addQuote = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const admin = await adminAuth(user);
      if (admin) {
        const text = 'INSERT INTO quotes(text) values($1) RETURNING text';
        const values = [req.body.text];
        const status = await db.query(text, values);
        if (status.rowCount) {
          res.send(`Quote '${status.rows[0].text}' has been added!`);
        } else {
          res.send('Quote was not added.');
        }
      } else {
        res.send(message.notAdmin);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// Gets a random quote
const getQuote = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const text = 'SELECT * FROM quotes ORDER BY RANDOM() LIMIT 1';
      const quote = await db.query(text);
      if (quote.rowCount) {
        res.send(quote.rows[0]);
      } else {
        res.send(message.noQuote);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// [Admin only] Gets all quotes
const getAllQuotes = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const admin = await adminAuth(user);
      if (admin) {
        const text = 'SELECT * FROM quotes';
        const status = await db.query(text);
        if (status.rowCount) {
          res.send(status.rows);
        } else {
          res.send(message.noQuote);
        }
      } else {
        res.send(message.notAdmin);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// [Admin only] Updates a specified quote
const updateQuote = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const admin = await adminAuth(user);
      if (admin) {
        const text = 'UPDATE quotes SET text=$1 WHERE id=$2 RETURNING text';
        const values = [req.body.text, req.params.id];
        const status = await db.query(text, values);
        if (status.rowCount) {
          res.send(`Task '${status.rows[0].text}' has been updated.`);
        } else {
          res.send(message.noQuote);
        }
      } else {
        res.send(message.notAdmin);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// [Admin only] Deletes a specified quote
const deleteQuote = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const admin = await adminAuth(user);
      if (admin) {
        const text = 'DELETE FROM quotes WHERE id=$1 RETURNING text';
        const values = [req.params.id];
        const status = await db.query(text, values);
        if (status.rowCount) {
          res.send(`Quote '${status.rows[0].text}' has been deleted.`);
        } else {
          res.send(message.noQuote);
        }
      } else {
        res.send(message.notAdmin);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

quoteRouter.post('/add', addQuote);
quoteRouter.get('/', getQuote);
quoteRouter.get('/all', getAllQuotes);
quoteRouter.put('/:id/update', updateQuote);
quoteRouter.delete('/:id/delete', deleteQuote);
