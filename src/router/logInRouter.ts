// eslint-disable-next-line no-unused-vars
import { Router, Request, Response } from 'express';
import db from '../database/index';

const loginRouter = Router();

export default loginRouter;

// Checks for a username's existence in the database
const confirmUsername = async (username: string) => {
  const text: string = 'SELECT username FROM users WHERE username=($1)';
  const values: Array<string> = [username];
  const userExists = await db.query(text, values);
  return userExists.rowCount > 0;
};

// Creates a new user in the database
const createUser = async (body: {username: string, password: string}) => {
  const { username, password } = body;
  const text: string = 'INSERT INTO users(username, password) VALUES($1, $2) RETURNING username';
  const values: Array<string> = [username, password];
  const user = await db.query(text, values);
  return user.rows[0].username;
};

// Registers user for application
const signUp = async (req: Request, res: Response) => {
  const userExists = await confirmUsername(req.body.username);
  if (userExists) {
    res.send('The username you entered already exists. Please choose another.');
  } else {
    const user = await createUser(req.body);
    res.send(`You can now sign in with username: ${user}`);
  }
};

// Logs user in if credentials are correct.
const logIn = async (req: Request, res: Response) => {
  const text: string = 'SELECT username FROM users WHERE username=($1) AND password=($2)';
  const values: Array<string> = [req.body.username, req.body.password];
  const user = await db.query(text, values);
  if (user.rowCount) {
    if (req.session) {
      req.session.username = Buffer.from(req.body.username);
      req.session.password = Buffer.from(req.body.password);
    }
    res.send('You are now logged in!');
  } else {
    const userExists = await confirmUsername(req.body.username);
    if (userExists) {
      res.send('The password is incorrect.');
    } else {
      res.send(`The username ${values[0]} does not exist. Please create an account.`);
    }
  }
};

const logOut = async (req: Request, res: Response) => {
  if (req.session) {
    if (req.session.username && req.session.password) {
      delete req.session.username;
      delete req.session.username;
      res.send('You have now logged out.');
    } else {
      res.send('You are not logged in.');
    }
  } else {
    res.send('No session in place.');
  }
};

loginRouter.post('/signup', signUp);
loginRouter.post('/login', logIn);
loginRouter.post('/logout', logOut);
