import supertest from "supertest";
import app from '../../index';

describe('Auth Router', () => {
  it('authorizes', async () => {
    const res: any = await supertest(app).post('/user/auth');
    expect(res.statusCode).toEqual(200);
  });
});
