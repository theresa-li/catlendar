import supertest from "supertest";
import app from '../../index';
import db from '../../database/index';
import adminAuth from "../../service/adminAuthService";

jest.mock('../../database/index');

describe('Quote Router', () => {
  it('adds quote', async () => {
    db.query.mockResolvedValue({ rows: [{ text: 'quote' }] });
    const res: any = await supertest(app).post('/api/quotes/add');
    expect(res.statusCode).toEqual(200);
  });
  it('adds quote if user  is admin', async () => {
    db.query.mockResolvedValue({ rows: [{ text: 'quote' }] });
    const res: any = await supertest(app).post('/api/quotes/add');
    expect(res.statusCode).toEqual(200);
  });
  it('gets quote', async () => {
    db.query.mockResolvedValue({ rowCount: 1 });
    const res: any = await supertest(app).get('/api/quotes');
    expect(res.statusCode).toEqual(200);
  });
  it('gets all quotes', async () => {
    db.query.mockResolvedValue({ rowCount: 3 });
    const res: any = await supertest(app).get('/api/quotes/all');
    expect(res.statusCode).toEqual(200);
  });
  it('updates quote', async () => {
    db.query.mockResolvedValue({ rowCount: 1 });
    const res: any = await supertest(app).put('/api/quotes/1/update');
    expect(res.statusCode).toEqual(200);
  });
  it('deletes quote', async () => {
    db.query.mockResolvedValue({ rows: [{ text: 'quote' }] });
    const res: any = await supertest(app).delete('/api/quotes/1/delete');
    expect(res.statusCode).toEqual(200);
  });
});
