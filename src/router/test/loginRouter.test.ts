import supertest from "supertest";
import app from '../../index';
import db from '../../database/index';

jest.mock('../../database/index');

describe('Login Router', () => {
  it('signs up', async () => {
    db.query.mockResolvedValue({ rows: [{ username: 'username' }] });
    const res: any = await supertest(app).post('/user/signup');
    expect(res.statusCode).toEqual(200);
  });
  it('logs in', async () => {
    db.query.mockResolvedValue({ rowCount: 1 });
    const res: any = await supertest(app).post('/user/login')
      .send({
        username: 'username',
        password: 'password',
      });
    expect(res.statusCode).toEqual(200);
  });
  it('logs out', async () => {
    const res: any = await supertest(app).post('/user/logout');
    expect(res.statusCode).toEqual(200);
  });
});
