import supertest from "supertest";
import app from '../../index';
import db from '../../database/index';

jest.mock('../../database/index');

describe('Task Router', () => {
  it('adds task', async () => {
    db.query.mockResolvedValue({ rows: [{ name: 'task' }] });
    const res: any = await supertest(app).post('/api/tasks/add');
    expect(res.statusCode).toEqual(200);
  });
  it('gets all tasks', async () => {
    db.query.mockResolvedValue({ rowCount: 3, rows: [{}, {}, {}] });
    const res: any = await supertest(app).get('/api/tasks');
    expect(res.statusCode).toEqual(200);
  });
  it('gets task', async () => {
    db.query.mockResolvedValue({ rowCount: 1, rows: [{}] });
    const res: any = await supertest(app).get('/api/tasks/id');
    expect(res.statusCode).toEqual(200);
  });
  it('updates task', async () => {
    db.query.mockResolvedValue({ rowCount: 1, rows: [{ name: 'task' }] });
    const res: any = await supertest(app).put('/api/tasks/1/update');
    expect(res.statusCode).toEqual(200);
  });
  it('deletes task', async () => {
    db.query.mockResolvedValue({ rows: [{ name: 'task' }] });
    const res: any = await supertest(app).delete('/api/tasks/1/delete');
    expect(res.statusCode).toEqual(200);
  });
});
