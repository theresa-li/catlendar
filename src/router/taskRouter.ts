// eslint-disable-next-line no-unused-vars
import { Router, Request, Response } from 'express';
import authenticate from '../service/authService';
import db from '../database/index';
import message from '../constants/messageConstants';

const taskRouter = Router();

export default taskRouter;

// Creates a new task in the database
const addTask = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const text = 'INSERT INTO tasks(user_id, name, start_date, end_date) VALUES($1, $2, $3, $4) RETURNING name';
      const values: Array<string | null> = [user, req.body.name, req.body.start, req.body.end];
      const task = await db.query(text, values);
      if (task.rowCount) {
        res.send(`Task '${task.rows[0].name}' has been added!`);
      } else {
        res.send('Task was not added.');
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// Gets all of logged in user's tasks from the database
const getTasks = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const text = 'SELECT * FROM tasks WHERE user_id=$1 ORDER BY start_date';
      const values = [user];
      const tasks = await db.query(text, values);
      if (tasks.rowCount) {
        res.send(tasks.rows);
      } else {
        res.send(message.noTask);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// Gets a user's specified task from the database
const getTask = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const text = 'SELECT * FROM tasks WHERE user_id=$1 AND id=$2';
      const values = [user, req.params.id];
      const tasks = await db.query(text, values);
      if (tasks.rowCount) {
        res.send(tasks.rows[0]);
      } else {
        res.send(message.noTask);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

// Updates an existing task in the database
const updateTask = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const {
        name, start, end, complete,
      } = req.body;
      const text = 'UPDATE tasks SET name=$1, start_date=$2, end_date=$3, complete=$4 WHERE user_id=$5 AND id=$6 RETURNING name';
      const values = [name, start, end, complete, user, req.params.id];
      const update = await db.query(text, values);
      if (update.rowCount) {
        res.send(`Task '${update.rows[0].name}' has been updated.`);
      } else {
        res.send(message.noTask);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

const deleteTask = async (req: Request, res: Response) => {
  if (req.session) {
    const user = await authenticate(req.session);
    if (user) {
      const text = 'DELETE FROM tasks WHERE user_id=$1 AND id=$2 RETURNING name';
      const values = [user, req.params.id];
      const task = await db.query(text, values);
      if (task.rowCount) {
        res.send(`Task '${task.rows[0].name}' has been deleted.`);
      } else {
        res.send(message.noTask);
      }
    } else {
      res.send(message.loggedOut);
    }
  } else {
    res.send(message.noSession);
  }
};

taskRouter.post('/add', addTask);
taskRouter.get('/', getTasks);
taskRouter.get('/:id', getTask);
taskRouter.put('/:id/update', updateTask);
taskRouter.delete('/:id/delete', deleteTask);
