// eslint-disable-next-line no-unused-vars
import { Router, Request, Response } from 'express';
import authenticate from '../service/authService';

const authRouter: Router = Router();

export default authRouter;

// For development purposes
// Confirms that authentication is working
const authorize = async (req: Request, res: Response) => {
  if (req.session) {
    const user = req.session;
    const status = await authenticate(user);
    if (status) {
      res.send('Authorized');
    } else {
      res.send('Access denied');
    }
  } else {
    res.send('No session in place.');
  }
};

authRouter.post('/auth', authorize);
