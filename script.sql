CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar NOT NULL,
  "password" varchar NOT NULL,
  "admin" boolean DEFAULT false
);

CREATE TABLE "tasks" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  "name" varchar NOT NULL,
  "start_date" date NOT NULL,
  "end_date" date,
  "complete" boolean DEFAULT false
);

CREATE TABLE "quotes" (
  "id" SERIAL PRIMARY KEY,
  "text" varchar NOT NULL
);

ALTER TABLE "tasks" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
