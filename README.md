# Catlendar
Welcome to a potentially cat themed calendar that'll brighten your day! It also lets you plan out your days, but that's not as important.
Access my backend server at `3.23.131.200:3000`!

## Installation
1. Clone the repository: `git clone https://gitlab.com/theresa-li/catlendar.git`
2. Install dependencies: `npm install`
3. Set up Postgres database with 'script.sql' file.
4. Create ENV variables `DB_USER` and `DB_PASSWORD` for database connection.

##### For Development:
1. Start express server: `npm run dev`

##### For production:
1. Transpile files: `npm run build`
2. Start express server: `npm run start`

## Running tests
Coming soon!

## Goals
- [x] Create a RESTful API/Web Service using JavaScript (and TypeScript)
- [x] Documentation
    - Ask yourself if another person can understand how your app works without you having to explain it to them
- [x] Unit Testing
    - Ideally at least a unit test per endpoint
- [x] More than one type of role that you can log in as
    - i.e. some sort of user and an admin roles
- [x] Login Functionality
- [x] You should be able to find data by different criteria
    - i.e. on an e-commerce website, being able to find an order by id number, or by user, or by status, etc.
- [x] Data is persistant in a database
- [x] Use at least 2 database tables
- [x] User input
- [ ] Logging

## Stretch Goals
- [ ] Password Hashing
- [ ] Use JSON Web Tokens instead of session storage
- [ ] (Paging and sorting endpoints)[https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-design#filter-and-paginate-data]
- [ ] Allow your application to allow the storage of images